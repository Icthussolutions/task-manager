const sgMail = require('@sendgrid/mail')


sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'andy@icthussolutions.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the App, ${name}. Let me know how you get along with the app.`
    })
}

const sendCancelationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'andy@icthussolutions.com',
        subject: 'Sad to see you go!',
        text: `Hi ${name}, we are sorry you have decided to leave. Please feel free to give us some feedback on why you have decided to go`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail
}